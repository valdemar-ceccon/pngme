use std::fmt::Display;

use crc::CRC_32_ISO_HDLC;

use crate::{chunk_type::ChunkType, Error, Result};

#[derive(Debug)]
pub struct Chunk {
    len: u32,
    typ: ChunkType,
    data: Vec<u8>,
    crc: u32,
}

impl Chunk {
    pub fn new(chunk_type: ChunkType, data: Vec<u8>) -> Self {
        let crc_calc = crc::Crc::<u32>::new(&CRC_32_ISO_HDLC);

        let mut dig = crc_calc.digest();

        dig.update(&chunk_type.bytes());
        dig.update(&data);
        let calculated_crc = dig.finalize();

        Self {
            len: data.len() as u32,
            typ: chunk_type,
            data: data,
            crc: calculated_crc,
        }
    }

    pub fn length(&self) -> u32 {
        self.len
    }

    pub fn chunk_type(&self) -> &ChunkType {
        &self.typ
    }

    // pub fn data(&self) -> &[u8] {
    //     &self.data
    // }

    pub fn crc(&self) -> u32 {
        self.crc
    }

    pub fn data_as_string(&self) -> Result<String> {
        match String::from_utf8(self.data.clone()) {
            Ok(v) => Ok(v),
            Err(e) => Err(Box::new(e)),
        }
    }

    pub fn as_bytes(&self) -> Vec<u8> {
        self.len
            .to_be_bytes()
            .iter()
            .chain(self.typ.bytes().iter())
            .chain(self.data.clone().iter())
            .chain(self.crc.to_be_bytes().iter())
            .copied()
            .collect()
    }
}

#[derive(Debug)]
struct InvalidChunkError {
    calculated: u32,
    informed: u32,
}

impl InvalidChunkError {
    fn new(calc: u32, inf: u32) -> Self {
        InvalidChunkError {
            calculated: calc,
            informed: inf,
        }
    }
}

impl Display for InvalidChunkError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "InvalidChunkError{{ calc: {} inf: {} }}",
            self.calculated, self.informed
        )
    }
}

impl std::error::Error for InvalidChunkError {}

impl TryFrom<&[u8]> for Chunk {
    type Error = Error;

    fn try_from(value: &[u8]) -> Result<Self> {
        let len_bytes: [u8; 4] = value[0..4].try_into()?;
        let len = u32::from_be_bytes(len_bytes) as usize;
        let typ_bytes: [u8; 4] = value[4..8].try_into()?;
        let crc_bytes: [u8; 4] = value[len + 8..].try_into()?;
        let mut data: Vec<u8> = Vec::with_capacity(len);

        for c in value[8..len + 8].iter() {
            data.push(*c);
        }

        let ret = Chunk {
            typ: ChunkType::try_from(typ_bytes)?,
            data: data,
            len: len as u32,
            crc: u32::from_be_bytes(crc_bytes),
        };

        let crc_calc = crc::Crc::<u32>::new(&CRC_32_ISO_HDLC);

        let mut dig = crc_calc.digest();

        dig.update(&value[4..len + 8]);
        let calculated_crc = dig.finalize();

        if calculated_crc != ret.crc {
            return Err(Box::new(InvalidChunkError::new(calculated_crc, ret.crc)));
        }

        Ok(ret)
    }
}

impl Display for Chunk {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Chunk {{")?;
        write!(f, "  length = {}", self.length())?;
        write!(f, "  chunk_type = {}", self.chunk_type())?;
        write!(f, "  crc = {}", self.crc())?;
        write!(f, "}}")
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn testing_chunk() -> Chunk {
        let data_length: u32 = 42;
        let chunk_type = "RuSt".as_bytes();
        let message_bytes = "This is where your secret message will be!".as_bytes();
        let crc: u32 = 2882656334;

        let chunk_data: Vec<u8> = data_length
            .to_be_bytes()
            .iter()
            .chain(chunk_type.iter())
            .chain(message_bytes.iter())
            .chain(crc.to_be_bytes().iter())
            .copied()
            .collect();

        Chunk::try_from(chunk_data.as_ref()).unwrap()
    }

    #[test]
    fn test_chunk_length() {
        let chunk = testing_chunk();
        assert_eq!(chunk.length(), 42);
    }

    #[test]
    fn test_chunk_type() {
        let chunk = testing_chunk();
        assert_eq!(chunk.chunk_type().to_string(), String::from("RuSt"));
    }

    #[test]
    fn test_chunk_string() {
        let chunk = testing_chunk();
        let chunk_string = chunk.data_as_string().unwrap();
        let expected_chunk_string = String::from("This is where your secret message will be!");
        assert_eq!(chunk_string, expected_chunk_string);
    }

    #[test]
    fn test_chunk_crc() {
        let chunk = testing_chunk();
        assert_eq!(chunk.crc(), 2882656334);
    }

    #[test]
    fn test_valid_chunk_from_bytes() {
        let data_length: u32 = 42;
        let chunk_type = "RuSt".as_bytes();
        let message_bytes = "This is where your secret message will be!".as_bytes();
        let crc: u32 = 2882656334;

        let chunk_data: Vec<u8> = data_length
            .to_be_bytes()
            .iter()
            .chain(chunk_type.iter())
            .chain(message_bytes.iter())
            .chain(crc.to_be_bytes().iter())
            .copied()
            .collect();

        let chunk = Chunk::try_from(chunk_data.as_ref()).unwrap();

        let chunk_string = chunk.data_as_string().unwrap();
        let expected_chunk_string = String::from("This is where your secret message will be!");

        assert_eq!(chunk.length(), 42);
        assert_eq!(chunk.chunk_type().to_string(), String::from("RuSt"));
        assert_eq!(chunk_string, expected_chunk_string);
        assert_eq!(chunk.crc(), 2882656334);
    }

    #[test]
    fn test_invalid_chunk_from_bytes() {
        let data_length: u32 = 42;
        let chunk_type = "RuSt".as_bytes();
        let message_bytes = "This is where your secret message will be!".as_bytes();
        let crc: u32 = 2882656333;

        let chunk_data: Vec<u8> = data_length
            .to_be_bytes()
            .iter()
            .chain(chunk_type.iter())
            .chain(message_bytes.iter())
            .chain(crc.to_be_bytes().iter())
            .copied()
            .collect();

        let chunk = Chunk::try_from(chunk_data.as_ref());

        assert!(chunk.is_err());
    }

    #[test]
    pub fn test_chunk_trait_impls() {
        let data_length: u32 = 42;
        let chunk_type = "RuSt".as_bytes();
        let message_bytes = "This is where your secret message will be!".as_bytes();
        let crc: u32 = 2882656334;

        let chunk_data: Vec<u8> = data_length
            .to_be_bytes()
            .iter()
            .chain(chunk_type.iter())
            .chain(message_bytes.iter())
            .chain(crc.to_be_bytes().iter())
            .copied()
            .collect();

        let chunk: Chunk = TryFrom::try_from(chunk_data.as_ref()).unwrap();

        let _chunk_string = format!("{}", chunk);
    }
}
