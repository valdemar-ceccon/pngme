use std::{
    fs::File,
    io::{Read, Write},
    path::PathBuf,
    str::FromStr,
    vec,
};

use pngme::{
    chunk, chunk_type,
    png::{self, Png},
};

use crate::Result;

pub trait Command<T> {
    fn exec(&self) -> Result<T>;
}

pub struct Decode<'a> {
    path: &'a PathBuf,
    chunk_type: &'a str,
}

pub struct Print<'a> {
    path: &'a PathBuf,
}
impl<'a> Print<'a> {
    pub fn new(path: &'a PathBuf) -> Self {
        Self { path: path }
    }
}
pub struct Remove<'a> {
    path: &'a PathBuf,
    chunk_type: &'a str,
}

impl<'a> Decode<'a> {
    pub fn new(path: &'a PathBuf, chunk_type: &'a str) -> Self {
        Self {
            path: path,
            chunk_type: chunk_type,
        }
    }
}

impl<'a> Command<String> for Decode<'a> {
    fn exec(&self) -> Result<String> {
        let p = read_png(self.path)?;

        let c = p.chunk_by_type(self.chunk_type)?;

        Ok(c.data_as_string()?)
    }
}

impl<'a> Command<Vec<String>> for Print<'a> {
    fn exec(&self) -> Result<Vec<String>> {
        let p = read_png(self.path)?;
        Ok(p.chunks()
            .into_iter()
            .map(|f| f.chunk_type().to_string())
            .collect())
    }
}

impl<'a> Remove<'a> {
    pub fn new(path: &'a PathBuf, chunk_type: &'a str) -> Self {
        Self {
            path: path,
            chunk_type: chunk_type,
        }
    }
}

impl<'a> Command<String> for Remove<'a> {
    fn exec(&self) -> Result<String> {
        let mut p = read_png(self.path)?;

        let c = p.remove_chunk(self.chunk_type)?;
        save_png(self.path, &p)?;
        Ok(c.data_as_string()?)
    }
}

pub struct Encode<'a> {
    path: &'a PathBuf,
    chunk_type: &'a str,
    message: &'a str,
    output_file: &'a Option<PathBuf>,
}

impl<'a> Encode<'a> {
    pub fn new(
        path: &'a PathBuf,
        chunk_type: &'a str,
        message: &'a str,
        output_file: &'a Option<PathBuf>,
    ) -> Self {
        Self {
            path: path,
            chunk_type: chunk_type,
            message: message,
            output_file: output_file,
        }
    }
}

fn read_png(path: &PathBuf) -> Result<Png> {
    let mut f = File::open(path)?;
    let mut file_content = vec![];
    f.read_to_end(&mut file_content)?;
    let p = png::Png::try_from(&file_content[..])?;
    Ok(p)
}

fn save_png(path: &PathBuf, png_content: &png::Png) -> Result<()> {
    let mut f = File::create(path)?;
    f.write_all(&png_content.as_bytes())?;
    Ok(())
}

impl Command<()> for Encode<'_> {
    fn exec(&self) -> Result<()> {
        let output_file = self.output_file.clone().unwrap_or(self.path.clone());

        let mut p = read_png(self.path)?;

        match p.chunk_by_type(self.chunk_type) {
            Ok(_) => {
                p.remove_chunk(self.chunk_type)?;
            }
            Err(_) => {}
        }
        let chunk_type = chunk_type::ChunkType::from_str(self.chunk_type)?;
        let new_chunk = chunk::Chunk::new(chunk_type, self.message.as_bytes().clone().to_vec());
        p.append_chunk(new_chunk);
        save_png(&output_file, &p)
    }
}
