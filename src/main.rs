use clap::Parser;
use commands::Command;

mod args;
mod commands;

pub type Error = Box<dyn std::error::Error>;
pub type Result<T> = std::result::Result<T, Error>;

fn main() -> Result<()> {
    let args = args::Cli::parse();

    match &args.command {
        args::Commands::Encode {
            path,
            chunk_type,
            message,
            output_file,
        } => {
            let enc = commands::Encode::new(path, chunk_type, message, output_file);
            enc.exec()?;
        }
        args::Commands::Decode { chunk_type, path } => {
            let dec = commands::Decode::new(path, chunk_type);
            let msg = dec.exec()?;
            println!("{}", msg);
        }
        args::Commands::Remove { chunk_type, path } => {
            let dec = commands::Remove::new(path, chunk_type);
            let msg = dec.exec()?;
            println!("{}", msg);
        }
        args::Commands::Print { path } => {
            let dec = commands::Print::new(path);
            let c = dec.exec()?;
            for s in c {
                println!("{}", s);
            }
        }
    }

    Ok(())
}
