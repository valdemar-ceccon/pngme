use std::path::PathBuf;

use clap::{AppSettings, Parser, Subcommand};

/// A fictional versioning CLI
#[derive(Parser)]
#[clap(name = "git")]
#[clap(about = "A fictional versioning CLI")]
pub struct Cli {
    #[clap(subcommand)]
    pub command: Commands,
}

#[derive(Subcommand)]
pub enum Commands {
    /// encode the message into the image file
    #[clap(setting(AppSettings::ArgRequiredElseHelp))]
    Encode {
        /// Image path
        #[clap(required = true, parse(from_os_str))]
        path: PathBuf,
        /// PNG chunck type to insert the message into
        chunk_type: String,
        /// Message to encode
        message: String,
        /// If present, creates a new image with the message
        #[clap(required = false, parse(from_os_str))]
        output_file: Option<PathBuf>,
    },
    /// decode the message for a given chunk type
    #[clap(setting(AppSettings::ArgRequiredElseHelp))]
    Decode {
        /// Image path
        #[clap(required = true, parse(from_os_str))]
        path: PathBuf,
        /// Fetch the message from the chunk type
        chunk_type: String,
    },
    /// Remove the message from the chunk type
    #[clap(setting(AppSettings::ArgRequiredElseHelp))]
    Remove {
        /// Image path
        #[clap(required = true, parse(from_os_str))]
        path: PathBuf,
        /// Chunk type to remove the message from
        chunk_type: String,
    },
    /// Print message contained in the image
    #[clap(setting(AppSettings::ArgRequiredElseHelp))]
    Print {
        /// Image path
        #[clap(required = true, parse(from_os_str))]
        path: PathBuf,
    },
}
